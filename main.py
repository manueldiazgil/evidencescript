# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import os

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.
    # Ask for initial name
    name = input("Enter the initials of your name: ")
    # Ask for the route
    route = input("Enter the route you want to take: ")
    #Ask for the date of the evidence
    date = input("Enter the date of the evidence: ")
    # Add Initial to every file in the route and date at the and
    for filename in os.listdir(route):
        # Split name and extension
        filename, file_extension = os.path.splitext(filename)
        dst = name + filename + date
        src = f'{route}/{filename}{file_extension}'
        dst = f'{route}/{dst}{file_extension}'
        # rename() function will
        # rename all the files
        os.rename(src, dst)
    print("All files renamed successfully")


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
